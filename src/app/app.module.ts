import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CharactersComponent } from './components/characters/characters.component';
import { LinksComponent } from './components/links/links.component';
import { FoodsComponent } from './components/foods/foods.component';

@NgModule({
  declarations: [AppComponent, CharactersComponent, LinksComponent, FoodsComponent],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
